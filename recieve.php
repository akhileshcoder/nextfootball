<?php
if (!function_exists('json_encode')) {
    function json_encode($a = false)
    {
        if (is_null($a)) return 'null';
        if ($a === false) return 'false';
        if ($a === true) return 'true';
        if (is_scalar($a)) {
            if (is_float($a)) {
                // Always use "." for floats.
                return floatval(str_replace(",", ".", strval($a)));
            }

            if (is_string($a)) {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
            } else
                return $a;
        }
        $isList = true;
        for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
            if (key($a) !== $i) {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList) {
            foreach ($a as $v) $result[] = json_encode($v);
            return '[' . join(',', $result) . ']';
        } else {
            foreach ($a as $k => $v) $result[] = json_encode($k) . ':' . json_encode($v);
            return '{' . join(',', $result) . '}';
        }
    }
}
$servername = '13.57.226.11';
$username = 'nutanix';
$password = 'Nutanix/4u';
$con = new mysqli($servername, $username, $password);
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}
function insert_product(&$con){
    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body, true);
    $clear_d = $data['clearDta'];
    $date_d = $data['date'];
    $rank_d = $data['rank'];
    $team_d = $data['team'];
    if (!(empty($clear_d) && empty($date_d) && empty($rank_d) && empty($team_d))) {
        if($clear_d === "no") {
            $sql = "UPDATE user.PREMIER_LEAGUE SET WON = $team_d WHERE DATE = $date_d AND RANK = $rank_d;";
        } else {
            $sql = "UPDATE user.PREMIER_LEAGUE SET WON = 0;";
        }
        if ($con->query($sql) === FALSE) {
            $response=array(
                'status' => 0,
                'status_message' =>'Product UPDATED Failed.',
                '$sql' =>$sql
            );
        }else {
            $response=array(
                'status' => 1,
                'status_message' =>'Product UPDATED Successfully.',
                '$sql' => $sql
            );
        }
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}
insert_product($con);
?>