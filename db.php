<html>
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/skeleton.css">
<link rel="stylesheet" type="text/css" href="css/logo.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<body>
<div class="sid-wrap"></div>
<div class="body-wrapper">
    <div class="sidebar">
        <div class="li profile">
            <img src="img/profile.png" alt="">
        </div>
        <div class="li selected">
            <img src="img/menu1.png" alt="">
        </div>
        <div class="li">
            <img src="img/menu2.png" alt="">
        </div>
        <div class="li">
            <img src="img/menu3.png" alt="">
        </div>
        <div class="li">
            <img src="img/menu4.png" alt="">
        </div>
        <div class="li">
            <img src="img/menu5.png" alt="">
        </div>
    </div>
    <div class="page-content"></div>
</div>
<script type="application/javascript" src="js/script.js"></script>
<?php
if (!function_exists('json_encode')) {
    function json_encode($a = false)
    {
        if (is_null($a)) return 'null';
        if ($a === false) return 'false';
        if ($a === true) return 'true';
        if (is_scalar($a)) {
            if (is_float($a)) {
                // Always use "." for floats.
                return floatval(str_replace(",", ".", strval($a)));
            }

            if (is_string($a)) {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
            } else
                return $a;
        }
        $isList = true;
        for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
            if (key($a) !== $i) {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList) {
            foreach ($a as $v) $result[] = json_encode($v);
            return '[' . join(',', $result) . ']';
        } else {
            foreach ($a as $k => $v) $result[] = json_encode($k) . ':' . json_encode($v);
            return '{' . join(',', $result) . '}';
        }
    }
}
function readFil($file_path){
    $myfile = fopen($file_path, "r") or die("Unable to open file!");
    $data =  fread($myfile,filesize($file_path));
    fclose($myfile);
    return $data;
}
$servername = '13.57.226.11';
$username = 'nutanix';
$password = 'Nutanix/4u';
$con = new mysqli($servername, $username, $password);
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}
try {
    $sql1 = readFil("sql_g_schema.sql");
    $result1 = $con->query($sql1);
    $schma = $result1->num_rows;
    /*$sql8 =  readFil("sql_d_league.sql");
    $result8 = $con->query($sql8);
    $sql9 =  readFil("sql_d_team.sql");
    $result9 = $con->query($sql9);*/
    if (empty($schma) || $schma === 0) {
        $sql2 =  readFil("sql_c_team.sql");
        $result2 = $con->query($sql2);
        $sql3 = readFil("sql_c_league.sql");

        $result3 = $con->query($sql3);
        $sql4 = readFil("sql_i_team.sql");
        $result4 = $con->query($sql4);
        $sql5 = readFil("sql_i_league.sql");
        $result5 = $con->query($sql5);
    }
} catch (Exception $e) {
    echo '3 wrg: ' . $e->getMessage();
}
function getData(&$con, $file_pth)
{
    try {
        $result = $con->query(readFil($file_pth));
        $rows = array();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
        }
        $data = json_encode($rows);
        echo $data;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function closeCon(&$con){
    try{
        $con->close();
    }catch (Exception $e) {
        echo $e->getMessage();
    }
}

?>
<script type="text/javascript">
    (function (w) {
        var team = [],league = [], resp1, resp2;
        try {
            resp1 = '<?php getData($con, 'sql_s_team.sql'); ?>';
            resp1 && (team = JSON.parse(resp1))
        } catch (e) {
            console.log(e);
        }
        try {
            resp2 = '<?php getData($con, 'sql_s_league.sql'); ?>';
            resp2 && (league = JSON.parse(resp2))
        } catch (e) {
            console.log(e);
        }
        try {
            <?php closeCon($con); ?>
        } catch (e) {
            console.log(e);
        }
        var _clean = function(ar){
            return ar.map(function (i) {
                var i1 = {};
                for (var k in i){
                    if (i.hasOwnProperty(k)){
                        if((/[^0-9]/g).test(i[k])){
                            i1[k] = i[k];
                        }else {
                            i1[k] = parseInt(i[k]);
                        }
                    }
                }
                return i1
            });
        }
        w._league_team = {
            league: _clean(league), team: _clean(team)
        };
        setTimeout(function () {
            w.renderPageContent(league, team);
        },0);
    })(window)
</script>
</body>
</html>
