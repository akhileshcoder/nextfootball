(function (w) {
    var _sendWin = function (_date, _rank, _team, clearDta) {
        var date = parseInt(_date), rank=parseInt(_rank), team=parseInt(_team);
        w._league_team.league.forEach(function (i) {
            if(i.DATE === date && i.RANK === rank){
                i.WON = team+1;
            }
            if(clearDta){
                i.WON = 0;
            }
        });
        _sendData(date, rank, team+1, clearDta ? 'yes' : 'no');
        w.renderPageContent(w._league_team.league, w._league_team.team);
    };
    var _formatDate = function (date) {
        var days = ['Sunday','Monday','Tuesday','Wednesday',
            'Thursday','Friday','Saturday'];
        var monthNames = ['January','February','March','April',
            'May','June','July','August','September','October',
            'November','December'];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return days[ date.getDay() ]+ ', ' + day + ' ' + monthNames[monthIndex] + ' ' + year;
    };
    var _convertData = function(l, t){
        var team = {};
        t.forEach(function (i) {
            team[i.TEAM_ID] = {};
            team[i.TEAM_ID].name = i.NAME;
            team[i.TEAM_ID].shortName = i.SHORT_NAME;
            team[i.TEAM_ID].abbr = i.ABBR;
        });
        var obj = {};
        l.forEach(function (i) {
            obj[i.DATE] || (obj[i.DATE] = []);
            obj[i.DATE][i.RANK] || (obj[i.DATE][i.RANK] = {});
            obj[i.DATE][i.RANK].data = [team[i.TEAM_1], team[i.TEAM_2]]
            obj[i.DATE][i.RANK].won = parseInt(i.WON);
        });
        return obj;
    };
    _sendData = function (date, rank, team, clearDta) {
        var formData = {
            date: date, rank: rank, team: team, clearDta: clearDta
        };
        w.fetch('/recieve.php', {
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',  // sent request
                'Accept': 'application/json'   // expected data sent back
            },
            method: 'POST',
            body: JSON.stringify(formData)
        }).then(function (resp) {
            return resp.json()
        }).then(function (resp) {
            console.log('data: ', resp);
        }).catch((error) => console.log('error', error));
    };
    w.renderPageContent = function (league_d, team_d) {
        var pageContent = document.createElement('div');
        var data = _convertData(league_d, team_d);
        pageContent.classList.add("row-grp");
        Object.keys(data).sort().forEach(function (d) {
            var itm = data[d];
            var row = document.createElement('div');
            row.classList.add("row");
            var date = document.createElement("div");
            date.classList.add("date");
            var dt = _formatDate(new Date(typeof d === 'string' ? parseInt(d) : d));
            date.appendChild(document.createTextNode(dt));
            row.appendChild(date);
            var row1 = document.createElement('div');
            row1.classList.add("row");
            itm.forEach(function (m, ind) {
                var columns = document.createElement('div');
                columns.classList.add("columns", "four");

                var team = document.createElement('div');
                team.classList.add("team", "win-team-" + (m.won ? m.won : 'n'));
                m.data.forEach(function (t, ind1) {
                    var teami = document.createElement('div');
                    teami.classList.add('team' + (ind1 + 1));
                    var badge = document.createElement('div');
                    badge.classList.add("badge-70", t.abbr);
                    teami.appendChild(badge);
                    var label = document.createElement('div');
                    label.classList.add("label");
                    label.setAttribute("title", t.name);
                    label.appendChild(document.createTextNode(t.shortName));
                    teami.appendChild(label);
                    teami.addEventListener('click', function () {
                        _sendWin(d, ind, ind1);
                    });
                    team.appendChild(teami);
                });
                var divider = document.createElement('img');
                divider.classList.add('divider');
                divider.setAttribute('src', "img/vs" + (m.won ? m.won : 'n') + ".png");
                team.appendChild(divider);
                columns.appendChild(team);
                row1.appendChild(columns);
            });
            row.appendChild(row1);
            pageContent.appendChild(row);
        });
        var pageContentOrg = document.getElementsByClassName("page-content")[0];
        while (pageContentOrg.firstChild) {
            pageContentOrg.removeChild(pageContentOrg.firstChild);
        }
        pageContentOrg.appendChild(pageContent);
    };
    document.onkeyup = function(e) {
        if (e.ctrlKey && e.altKey && e.shiftKey && (e.which || e.keyCode || e.key) === 85) {
            console.log("Ctrl + Alt + Shift + U shortcut combination was pressed");
            _sendWin(0,0,0,true);
        }
    };
})(window);